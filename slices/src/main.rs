fn main() {
    first_word_index();
    string_slice();
    omit_start();
    omit_end();
    omit_both();
    first_word_slice();
    slice_parameter();
    other_slices();
}

#[allow(unused)]
fn first_word_index() {
    let mut s = String::from("hello world");
    let word = first_word_1(&s);
    s.clear();
}

fn first_word_1(s: &String) -> usize {
    let bytes = s.as_bytes();
    for(i, &item) in bytes.iter().enumerate() {
        if item == b' ' {
            return i;
        }
    }
    s.len()
}

fn string_slice() {
    let s = String::from("hello world");
    let hello = &s[0..5];
    let world = &s[6..11];
    println!("{hello} {world}");
}

#[allow(unused)]
fn omit_start() {
    let s = String::from("hello");
    let slice = &s[0..2];
    let slice = &s[..2];
}

#[allow(unused)]
fn omit_end() {
    let s = String::from("hello");
    let len = s.len();
    let slice = &s[3..len];
    let slice = &s[3..];
}

#[allow(unused)]
fn omit_both() {
    let s = String::from("hello");
    let len = s.len();
    let slice = &s[0..len];
    let slice = &s[..];
}

#[allow(unused)]
fn first_word_slice() {
    let mut s = String::from("hello world");
    let word = first_word_2(&s);
    // s.clear(); // this code does not compile!
    println!("the first word is: {word}");
}

fn first_word_2(s: &String) -> &str {
    first_word_3(&s)
}

#[allow(unused)]
fn slice_parameter() {
    let my_string = String::from("hello world");
    let word = first_word_3(&my_string[0..6]);
    let word = first_word_3(&my_string[..]);
    let word = first_word_3(&my_string);

    let my_string_literal = "hello world";
    let word = first_word_3(&my_string_literal[0..6]);
    let word = first_word_3(&my_string_literal[..]);
    let word = first_word_3(my_string_literal);
}

fn first_word_3(s: &str) -> &str {
    let bytes = s.as_bytes();
    for(i, &item) in bytes.iter().enumerate() {
        if item == b' ' {
            return &s[0..i];
        }
    }
    &s[..]
}

fn other_slices() {
    let a = [1, 2, 3, 4, 5];
    let slice = &a[1..3];
    assert_eq!(slice, &[2, 3]);
}
