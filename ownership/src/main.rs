fn main() {
    string_literal();
    string_mutable();
    integer_copy();
    string_move();
    string_clone();
    function_parameter();
    function_return();
    function_tuple();
}

#[allow(unused)]
fn string_literal() {
    let s = "hello";
    // do stuff with s
}

fn string_mutable() {
    let mut s = String::from("hello");
    s.push_str(", world");
    println!("{s}");
}

fn integer_copy() {
    let x = 5;
    let y = x;
    println!("x = {x}, y = {y}")
}

#[allow(unused)]
fn string_move() {
    let s1 = String::from("hello");
    let s2 = s1;
    // println!("{s1}, world!"); // this code does not compile!
}

fn string_clone() {
    let s1 = String::from("hello");
    let s2 = s1.clone();
    println!("s1 = {s1}, s2 = {s2}");
}

fn function_parameter() {
    let s = String::from("hello");
    takes_ownership(s);
    // println!("{s}"); // this code does not compile!
    let x = 5;
    makes_copy(x);
    println!("{x}");
}

fn takes_ownership(some_string: String) {
    println!("{some_string}");
}

fn makes_copy(some_integer: i32) {
    println!("{some_integer}");
}

#[allow(unused)]
fn function_return() {
    let s1 = gives_ownership();
    let s2 = String::from("hello");
    let s3 = takes_and_gives_back(s2);
}

fn gives_ownership() -> String {
    let some_string = String::from("yours");
    some_string
}

fn takes_and_gives_back(a_string: String) -> String {
    a_string
}

fn function_tuple() {
    let s1 = String::from("hello");
    let (s2, len) = calculate_length(s1);
    println!("The length of '{s2}' is {len}.");
}

fn calculate_length(s: String) -> (String, usize) {
    let length = s.len();
    (s, length)
}
