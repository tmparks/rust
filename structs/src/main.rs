fn main() {
    example1();
    let user1 = build_user(
        String::from("someone@example.com"),
        String::from("someusername123"),
    );
    println!(
        "{0} {1} {2} {3}",
        user1.active, user1.username, user1.email, user1.sign_in_count
    );
    tuple_struct();
    unit_struct();
}

struct User {
    active: bool,
    username: String,
    email: String,
    sign_in_count: u64,
}

#[allow(unused)]
fn example1() {
    let mut user1 = User {
        active: true,
        username: String::from("someusername123"),
        email: String::from("someone@example.com"),
        sign_in_count: 1,
    };
    user1.email = String::from("anotheremail@example.com");

    let user2 = User {
        email: String::from("another@example.com"),
        ..user1
    };
}

fn build_user(email: String, username: String) -> User {
    User {
        active: true,
        username,
        email,
        sign_in_count: 1,
    }
}

struct Color(i32, i32, i32);
struct Point(i32, i32, i32);

#[allow(unused)]
fn tuple_struct() {
    let black = Color(0, 0, 0);
    let origin = Point(0, 0, 0);
}

struct AlwaysEqual;

#[allow(unused)]
fn unit_struct() {
    let subject = AlwaysEqual;
}
