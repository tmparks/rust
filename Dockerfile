FROM ubuntu

# Install development tools
RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install --yes \
    git  \
    nano \
    rust-all \
    && rm --recursive --force /var/lib/apt/lists/*
