fn main() {
    reference_parameter();
    mutable_reference();
    multiple_mutable_references();
    mutable_and_immutable_references();
    dangling_references();
}

fn reference_parameter() {
    let s1 = String::from("hello");
    let len = calculate_length(&s1);
    println!("The length of '{s1}' is {len}.");
}

fn calculate_length(s: &String) -> usize {
    s.len()
}

fn mutable_reference() {
    let mut s = String::from("hello");
    change(&mut s);
    println!("{s}");
}

fn change(some_string: &mut String) {
    some_string.push_str(", world");
}

#[allow(unused)]
fn multiple_mutable_references() {
    let mut s = String::from("hello");
    {
        let r1 = &mut s;
    } // r1 goes out of scope here
    let r2 = &mut s;
}

fn mutable_and_immutable_references() {
    let mut s = String::from("hello");
    let r1 = &s; // no problem
    let r2 = &s; // no problem
    println!("{r1} and {r2}");
    // variables r1 and r2 will not be used after this point

    let r3 = &mut s; // no problem
    println!("{r3}");
}

#[allow(unused)]
fn dangling_references() {
    // let reference_to_nothing = dangle();
    let reference_to_something = no_dangle();
}

// fn dangle() -> &String {
//     let s = String::from("hello");
//     &s // this code does not compile!
// }

fn no_dangle() -> String {
    let s = String::from("hello");
    s
}
